module.exports = function(eleventyConfig) {
    eleventyConfig.addLayoutAlias('base', 'base.njk')

    eleventyConfig.addPassthroughCopy("css");
    eleventyConfig.addPassthroughCopy("img");
    eleventyConfig.addPassthroughCopy("js");
    eleventyConfig.addPassthroughCopy("vendor");
};
