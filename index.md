---
hashtag: "#GoingGodbold"
layout: base
groom: "Scott Godbold"
bride: "Emily Fogelsonger"
groomFirst: Scott
groomLast: Godbold
brideFirst: Emily
brideLast: Fogelsonger
receptionDate: "Due to the COVID-19 pandemic our Reception has been postponed"
contactEmail: IHaveAQuestion@goinggodbold.com
images:
  - redwings
  - barrecode
  - wedding1
  - tigers
  - wedding5
  - bighouse5k
registries:
  - name: Zola
    link: https://www.zola.com/registry/goinggodbold
    icon: far fa-heart
# Release segments of the site as they are ready
story: false
gallery: false
releaseDetails: false
releaseContact: true
releaseRegistry: false
comingSoon: false
---
